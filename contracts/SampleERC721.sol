// SPDX-License-Identifier: UNLICENSED


pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract SampleERC721 is ERC721 {
    /**
     */
    constructor() ERC721("test", "TST") {}

    /**
     */
    function mint(uint256 tokenId) public returns (bool) {
        _mint(msg.sender, tokenId);
        return true;
    }
}
