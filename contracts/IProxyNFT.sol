// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

/**
 * @dev Interface of the Proxy NFT standard as defined in the EIP.
 */
interface IProxyNFT {

    /**
     * @dev struct to store delegate token details
     *
     */
    struct Delegate {
        address contractAddress;
        uint32 tokenId;
        uint32 quantity;
        string metaverseName;
    }

    /**
     * @dev Emitted when one or more new delegate NFTs are added to a Proxy NFT 
     *
     */
    event Bundled(uint32 proxyTokenID, Delegate[] delegate, address ownerAddress);

    /**
     * @dev Emitted when one or more delegate NFTs are removed from a Proxy NFT 
     */
    event Unbundled(uint32 proxyTokenID, Delegate[] delegate);

    /**
     * @dev Accepts the tokenId of the Proxy NFT and returns an array of delegate token data
     */
    function delegateTokens(uint32 proxyTokenID) external view returns (Delegate[] memory);

    /**
     * @dev Removes one or more delegate NFTs from a Proxy NFT
     * This function accepts the delegate NFT details, and transfer those NFTs out of the Proxy NFT contract to the owner's wallet
     */
    function unBundle(Delegate[] memory delegate, uint32 proxyTokenID) external;

    /**
     * @dev Adds one or more delegate NFTs to a Proxy NFT
     * This function accepts the delegate NFT details, and transfers those NFTs to the Proxy NFT contract
     * Need to ensure that approval is given to this Proxy NFT contract for the delegate NFTs so that they can be transferred programmatically
     */
    function bundle(Delegate[] memory delegate, uint32 proxyTokenID) external;

    /**
     * @dev Initializes a new bundle, mints a Proxy NFT and assigns it to msg.sender
     * Returns the token ID of a new Proxy NFT
     * Note - When a new Proxy NFT is initialized, it is empty, it does not contain any delegate NFTs
     */
    function initBundle(Delegate[] memory delegate) external;
}
