// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "./ERC721Full.sol";
import "./IProxyNFT.sol";

contract MetalinqNFT is
    IProxyNFT,
    ReentrancyGuard,
    ERC721Full,
    ERC1155Holder,
    AccessControl
{
    bytes32 public constant BUNDLER_ROLE = keccak256("BUNDLER_ROLE");

    uint32 currentProxyTokenID;
    uint32 currentWearableId;
    string public description;
    uint32 public defaultRoyaltyPercentage;
    address public defaultRoyaltyAddress;
    address public mintFeesReceiverAddress;
    uint32 public wearableNFTsCount;
    string private _uri;
    address private _owner;
    address public constant metalinqFeeAddress = address(0);
    uint32 public constant protocolFeePercent = 0;

    /// @dev Struct to store Wearable Data
    struct Wearable {
        uint256 price;
        string ipfsHash;
        uint32 copies;
        uint32 mintCount;
    }

    /// @dev maps wearableId with  wearable details
    mapping(uint32 => Wearable) public wearablesData;
    /// @dev maps wearableId with delegates
    mapping(uint32 => Delegate[]) public wearableDelegates;
    /// @dev maps tokenId with  wearableId
    mapping(uint32 => uint32) private _proxyToWearableId;
    /// @dev maps proxyTokenId with its delegates 
    mapping(uint32 => Delegate[]) public proxyDelegates;
    /// @dev maps proxyTokenId with corresponding delegate balance
    mapping(uint32 => mapping(address => mapping(uint32 => uint32)))
        public tokenBalances;
    /// @dev maps user's address with corresponding wearableId and balance
    mapping(address => mapping(uint32 => uint32)) public wearableBalances;

    modifier onlyOwner() {
        _checkOwner();
        _;
    }
    event Mint(uint32 proxyTokenID, uint32 wearableId, address minter);

    constructor(
        string memory _collection_name,
        string memory _collection_symbol,
        string memory _description,
        uint32 _defaultRoyaltyPercentage,
        address _defaultRoyaltyAddress,
        address bundlerAddress,
        string memory _baseUri
    ) ERC721Full(_collection_name, _collection_symbol) {
        description = _description;
        require(_defaultRoyaltyPercentage <= getMaxBPS());
        require(_defaultRoyaltyAddress != address(0), "invalid royalty address");
        defaultRoyaltyPercentage = _defaultRoyaltyPercentage;
        defaultRoyaltyAddress = _defaultRoyaltyAddress;
        mintFeesReceiverAddress = msg.sender;
        _uri = _baseUri;
        _owner = msg.sender;
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(BUNDLER_ROLE, msg.sender);
        _setRoleAdmin(BUNDLER_ROLE, DEFAULT_ADMIN_ROLE);
        _setupRole(BUNDLER_ROLE, bundlerAddress);
    }

    function getAmount(uint256 bps, uint256 amount)
        public
        pure
        returns (uint256)
    {
        return ((amount * bps)/getMaxBPS());
    }

    function getMaxBPS() public pure returns (uint32) {
        return 10000;
    }

    function setDefaultRoyaltyPercentage(uint32 royaltyPercent)
        external
        onlyOwner
    {
        defaultRoyaltyPercentage = royaltyPercent;
    }

    function setDefaultRoyaltyAddress(address royaltyAddress)
        external
        onlyOwner
    {
        require(royaltyAddress != address(0), "invalid royalty address");
        defaultRoyaltyAddress = royaltyAddress;
    }

    function setMintFeesReceiverAddress(address _mintFeesReceiverAddress)
        external
        onlyOwner
    {
        require(_mintFeesReceiverAddress != address(0), "invalid address"); 
        mintFeesReceiverAddress = _mintFeesReceiverAddress;
    }

    function updateWearablePricesByBatch(
        uint32[] memory wearableIds,
        uint256[] memory prices
    ) external onlyOwner {
        require(wearableIds.length == prices.length, "array length mismatach");
        for (
            uint32 index = 0;
            index < wearableIds.length;
            index = index + 1
        ) {
            wearablesData[wearableIds[index]].price = prices[index];
        }
    }

    function getProxyTokenData(uint32 tokenId)
        external
        view
        returns (Delegate[] memory, Wearable memory)
    {
        return (
            proxyDelegates[tokenId],
            wearablesData[_proxyToWearableId[tokenId]]
        );
    }

    function delegateTokens(uint32 proxyTokenId)
        external
        view
        returns (Delegate[] memory)
    {
        return proxyDelegates[proxyTokenId];
    }

    function setWearableNFTsData(
        string[] memory ipfsHashes,
        uint256[] memory prices,
        Delegate[][] memory wearableDelegateData,
        uint32[] memory copies
    ) external onlyOwner {
        require(
            (ipfsHashes.length == prices.length) &&
                (prices.length == wearableDelegateData.length) &&
                (wearableDelegateData.length == copies.length),
            "array lengths mismatch"
        );
        uint32 _wearableCounter = wearableNFTsCount;
        for (
            uint32 index = 0;
            index < ipfsHashes.length;
            index = index + 1
        ) {
            uint32 wearableId = currentWearableId + 1;
            wearablesData[wearableId].ipfsHash = ipfsHashes[index];
            wearablesData[wearableId].price = prices[index];
            wearablesData[wearableId].copies = copies[index];
            wearablesData[wearableId].mintCount = 0;
            _validateAndStoreWearableDelegateData(
                wearableId,
                wearableDelegateData[index]
            );
            _wearableCounter = _wearableCounter + 1;
            currentWearableId = wearableId;
        }
        wearableNFTsCount = _wearableCounter;
    }

    function getNextTokenId() public view returns (uint32) {
        return currentProxyTokenID + 1;
    }

    /// @notice Query if a contract implements an interface
    /// @param interfaceId The interface identifier, as specified in ERC-165
    /// @dev Interface identification is specified in ERC-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceId` and
    ///  `interfaceId` is not 0xffffffff, `false` otherwise
    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(AccessControl, ERC721Full, ERC1155Receiver)
        returns (bool)
    {
        return
            AccessControl.supportsInterface(interfaceId) ||
            ERC721Full.supportsInterface(interfaceId);
    }

    function mint(uint32 wearableId) external payable nonReentrant {
        Wearable memory tokenData = wearablesData[wearableId];
        require(tokenData.copies != 0, "wearableId doesn't exist");
        require((tokenData.mintCount < tokenData.copies), "max tokens minted");
        uint256 wearablePrice = tokenData.price;
        require(msg.value == wearablePrice, "invalid amount");

        uint32 _tokenId = getNextTokenId();

        _proxyToWearableId[_tokenId] = wearableId;
        wearablesData[wearableId].mintCount = wearablesData[wearableId]
            .mintCount + 1;
        this.initBundle(wearableDelegates[wearableId]);
        _incrementProxyTokenID();
        _safeMint(msg.sender, _tokenId);
        _setTokenURI(_tokenId, Strings.toString(_tokenId));
        _distributeFees();

        emit Mint(_tokenId, wearableId, msg.sender);
    }

    function initBundle(Delegate[] memory delegateData) external {
        uint32 proxyTokenId = getNextTokenId();
        for (
            uint32 index = 0;
            index < delegateData.length;
            index = index + 1
        ) {
            proxyDelegates[proxyTokenId].push(delegateData[index]);
        }
    }

    function bundle(Delegate[] memory delegateData, uint32 proxyTokenID)
        external
        nonReentrant
    {
        require(
            hasRole(BUNDLER_ROLE, msg.sender) ||
                ownerOf(proxyTokenID) == msg.sender,
            "caller neither bundler nor owner"
        );
        for (uint32 i = 0; i < delegateData.length; i = i + 1) {
            _ensureBundlingConditions(delegateData[i], proxyTokenID);
            address contractAddress = delegateData[i].contractAddress;
            uint32 tokenId = delegateData[i].tokenId;
            uint32 quantity = delegateData[i].quantity;

            tokenBalances[proxyTokenID][contractAddress][
                tokenId
            ] = tokenBalances[proxyTokenID][contractAddress][tokenId] + quantity;

            if (_isERC721(contractAddress)) {
                ERC721Full erc721Instance = ERC721Full(contractAddress);
                erc721Instance.transferFrom(msg.sender, address(this), tokenId);
            } else if (_isERC1155(contractAddress)) {
                ERC1155Supply erc1155Instance = ERC1155Supply(contractAddress);
                erc1155Instance.safeTransferFrom(
                    msg.sender,
                    address(this),
                    tokenId,
                    quantity,
                    ""
                );
            }
        }
        emit Bundled(proxyTokenID, delegateData, ownerOf(proxyTokenID));
    }

    function unBundle(Delegate[] memory delegateData, uint32 proxyTokenID)
        external
        nonReentrant
    {
        require(
            (ownerOf(proxyTokenID) == msg.sender),
            "caller not proxy owner"
        );
        for (uint32 i = 0; i < delegateData.length; i = i + 1) {
            require(
                _ensureUnBundlingConditions(delegateData[i], proxyTokenID),
                "delegate not assigned to proxy"
            );
            address contractAddress = delegateData[i].contractAddress;
            uint32 tokenId = delegateData[i].tokenId;
            uint32 quantity = delegateData[i].quantity;

            _updateDelegateBalances(delegateData[i], proxyTokenID);

            if (_isERC721(contractAddress)) {
                ERC721Full erc721Instance = ERC721Full(contractAddress);
                erc721Instance.transferFrom(address(this), msg.sender, tokenId);
            } else if (_isERC1155(contractAddress)) {
                ERC1155Supply erc1155Instance = ERC1155Supply(contractAddress);
                erc1155Instance.safeTransferFrom(
                    address(this),
                    msg.sender,
                    tokenId,
                    quantity,
                    ""
                );
            }
        }
        emit Unbundled(proxyTokenID, delegateData);
    }

    function _ensureBundlingConditions(
        Delegate memory delegateData,
        uint32 proxyTokenID
    ) internal view {
        Delegate[] memory storedData = proxyDelegates[proxyTokenID];
        for (uint32 i = 0; i < storedData.length; i = i + 1) {
            if (
                delegateData.contractAddress == storedData[i].contractAddress &&
                delegateData.tokenId == storedData[i].tokenId
            ) {
                uint32 balance = tokenBalances[proxyTokenID][
                    delegateData.contractAddress
                ][delegateData.tokenId];
                require(
                    balance + delegateData.quantity <=
                        storedData[i].quantity,
                    "assigned delegate qty exceeds"
                );
                return;
            }
        }
        revert("delegate not assigned to proxy");
    }

    function _ensureUnBundlingConditions(
        Delegate memory delegateData,
        uint32 proxyTokenID
    ) internal view returns (bool) {
        Delegate[] memory storedData = proxyDelegates[proxyTokenID];
        for (uint32 i = 0; i < storedData.length; i = i + 1) {
            if (
                delegateData.contractAddress == storedData[i].contractAddress &&
                delegateData.tokenId == storedData[i].tokenId
            ) {
                uint32 balance = tokenBalances[proxyTokenID][
                    delegateData.contractAddress
                ][delegateData.tokenId];
                require(
                    delegateData.quantity <= balance,
                    "qty exceeds balance"
                );
                return true;
            }
        }
        return false;
    }

    function _updateDelegateBalances(
        Delegate memory delegateData,
        uint32 proxyTokenID
    ) internal {
        address contractAddress = delegateData.contractAddress;
        uint32 tokenId = delegateData.tokenId;
        tokenBalances[proxyTokenID][contractAddress][tokenId] = tokenBalances[
            proxyTokenID
        ][contractAddress][tokenId] - delegateData.quantity;
    }

    function _distributeFees() internal {
        uint256 commissionCost = ((msg.value * protocolFeePercent)/100);
        uint256 mintCost = msg.value - commissionCost;
        _sendAmount(mintCost, mintFeesReceiverAddress);
        _sendAmount(commissionCost, metalinqFeeAddress);
    }

    function _sendAmount(uint256 amount, address receiver) internal {
        if (amount > 0) {
            (bool success, ) = payable(receiver).call{value: amount}("");
            require(success, "Send amount Failure");
        }
    }

    function _validateAndStoreWearableDelegateData(
        uint32 wearableId,
        Delegate[] memory data
    ) internal {
        for (uint32 i = 0; i < data.length; i = i + 1) {
            bool isERC721 = _isERC721(data[i].contractAddress);
            if (isERC721) {
                require(data[i].quantity == 1, "ERC721 qty must be 1");
            }
            require(
                isERC721 || _isERC1155(data[i].contractAddress),
                "invalid contractaddress"
            );
            wearableDelegates[wearableId].push(data[i]);
        }
    }

    function _baseURI() internal view override returns (string memory) {
        return _uri;
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if the sender is not the owner.
     */
    function _checkOwner() internal view virtual {
        require(owner() == msg.sender, "Ownable: caller is not the owner");
    }

    function _isERC1155(address contractAddress) internal view returns (bool) {
        return IERC1155(contractAddress).supportsInterface(0xd9b67a26);
    }

    function _isERC721(address contractAddress) internal view returns (bool) {
        return IERC721(contractAddress).supportsInterface(0x80ac58cd);
    }

    function _incrementProxyTokenID() internal {
        currentProxyTokenID = currentProxyTokenID + 1;
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override(ERC721Full) {
        super._beforeTokenTransfer(from, to, tokenId);
        uint32 wearableId = _proxyToWearableId[uint32(tokenId)];
        if (from != address(0))
            wearableBalances[from][wearableId] = wearableBalances[from][
                wearableId
            ] - 1;
        wearableBalances[to][wearableId] = wearableBalances[to][wearableId] + 1;
    }
}
