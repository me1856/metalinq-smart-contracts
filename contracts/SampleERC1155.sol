// SPDX-License-Identifier: SEE LICENSE IN LICENSE
pragma solidity >=0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";

contract SampleERC1155 is ERC1155("testtoken") {
    function mint(
        address recipient,
        uint256[] calldata tokenId,
        uint256[] calldata amounts
    ) external {
        _mintBatch(recipient, tokenId, amounts, "");
    }
}
