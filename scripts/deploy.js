// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat");

async function main() {

  const MetalinqNFTContract = await ethers.getContractFactory("MetalinqNFT")
  console.log("deploying metalinq test contract");
  const metalinqNFTContract = await MetalinqNFTContract.deploy(
    "TestMetalinq", //name
    "TMT",  //symbol
    "description", //description
    100, //default royalty percentage in bps (1% -> 100, 5%-> 500)
    "0xB89655D499A6EbD9a33C159697EFDa91d7A10433", //royalty receieveraddress
    "0xB89655D499A6EbD9a33C159697EFDa91d7A10433", //bundler address 
    "test", //base uri example: https://metalinq.abc/
    {
      gasLimit: 20000000,
    },
  );
  await metalinqNFTContract.deployed();

  console.log(
    `deployed:`, metalinqNFTContract.address
  );
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
