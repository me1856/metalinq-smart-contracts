# Metalinq Wearables Launcher

This project implements EIP 5606 - Multiverse NFTs specific to a case of a launching digital wearables for fashion brands.
Please read the following EIP to understand more about this implementation - https://eips.ethereum.org/EIPS/eip-5606

HOWTO -

```shell
npx hardhat help
npx hardhat test
GAS_REPORT=true npx hardhat test
npx hardhat node
npx hardhat run scripts/deploy.js

```
## Setup
1. clone the repo.
2. run command
```
npm install
```
3. check hardhat.conifg.json 

## How to compile contracts on Hardhat  
run command
```
npx hardhat compile
```

## How to deploy contracts on Hardhat network 

1. update scripts/deploy.js as per requirement.
2. run command 
```
npx hardhat run scripts/deploy.js

```
## How to deploy contracts on Mumbai testnet

1. add .env file
```
POLYGON_MUMBAI_RPC_PROVIDER= <mubai_testnet_rpc_provider_url>
PRIVATE_KEY= <owner_private_key>
POLYGONSCAN_API_KEY= <polygon_scan_api_key>
``` 
2. update scripts/deploy.js as per requirement.
3. run command 
```
npx hardhat run scripts/deploy.js --network mumbai
```

## How to run test cases 
1. run command
```
npx hardhat test

```
