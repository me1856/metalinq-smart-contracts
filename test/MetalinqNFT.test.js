const {
    time,
    loadFixture,
  } = require("@nomicfoundation/hardhat-network-helpers");
  const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");
  const { expect } = require("chai");
  const { ethers } = require("hardhat");

  
  let bundler, owner, minter1, minter2;
  let bundlerAddress, minter1Address, minter2Address;
  let proxyNftContract, sampleERC721Contract, sampleERC1155Contract;
  let sampleERC721ContractAddress, sampleERC1155ContractAddress; 
  const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
  const baseUri = "https://abc/";
  let provider;
  let defaultRoyaltyPercent = 100; 
  let accounts;

  describe("Tests", () => {
    beforeEach(async () => {
      const ProxyNftContract = await ethers.getContractFactory("MetalinqNFT")
      accounts = await ethers.getSigners();
      provider = ethers.provider;
      bundler = accounts[3];
      bundlerAddress = accounts[3].address;
      owner = accounts[0];
      minter1 = accounts[1];
      minter2 = accounts[2];
      minter1Address = accounts[1].address;
      minter2Address = accounts[2].address;
      royaltyAddress =  accounts[4].address;
      
      proxyNftContract = await ProxyNftContract.deploy(
        "TestProxyNFT",
        "TMT",
        "description",
        defaultRoyaltyPercent,
        royaltyAddress,
        bundlerAddress,
        baseUri,
        {
          gasLimit: 21000000,
          gasPrice: 8000000000,
        },
      );
      await proxyNftContract.deployed();

      
      const SampleERC721Contract = await ethers.getContractFactory("SampleERC721");
      sampleERC721Contract = await SampleERC721Contract.deploy();
      await sampleERC721Contract.deployed();
      sampleERC721ContractAddress = sampleERC721Contract.address;
      
      await sampleERC721Contract.connect(minter1).mint(1, {from: minter1Address});
      await sampleERC721Contract.connect(minter1).mint(2, {from: minter1Address});
      await sampleERC721Contract.connect(minter2).mint(3, {from: minter2Address});
      await sampleERC721Contract.connect(minter2).mint(4, {from: minter2Address});
      await sampleERC721Contract.connect(bundler).mint(5, {from: bundlerAddress});
  
      const SampleERC1155Contract = await ethers.getContractFactory("SampleERC1155");
      sampleERC1155Contract = await SampleERC1155Contract.deploy();
      await sampleERC1155Contract.deployed();
      sampleERC1155ContractAddress = sampleERC1155Contract.address;
      await sampleERC1155Contract.connect(minter1).mint(minter1Address, [1,2,3], [5,5,5] , {from: minter1Address});
      await sampleERC1155Contract.connect(minter2).mint(minter2Address, [1,2,3], [5,5,5] , {from: minter2Address});
      await sampleERC1155Contract.connect(bundler).mint(bundlerAddress, [1,2,3], [5,5,5] , {from: bundlerAddress});
      await sampleERC1155Contract.mint(owner.address, [1,2,3], [5,5,5]);
    }); 
  
    describe("Check constructor executed successfully", () => {
      it("Contract deployed successfully ", async () => {
        console.log("address:", proxyNftContract.address);
        expect(true).to.eq(!!proxyNftContract);
      });
  
      it("Contract owner is sender", async () => {
        const _owner = await proxyNftContract.owner();
        expect(_owner).to.eq(owner.address);
      });

      it("throws if Zero address is default royalty address", async () => {
        const ProxyNftContract = await ethers.getContractFactory("MetalinqNFT")
        await expect(
            ProxyNftContract.deploy(
                "TestProxyNFT",
                "TMT",
                "description",
                defaultRoyaltyPercent,
                ZERO_ADDRESS,
                bundlerAddress,
                baseUri,
                {
                  gasLimit: 21000000,
                  gasPrice: 8000000000,
                },
              )).to.be.revertedWith('invalid royalty address');
      });
    });
  
    describe("utility functions", () => {
      it("get correct amount on sending bps and inputAmount", async () => {
        const inputAmount = 100;
        const percentInBPS = 500; //5%
        const amount = await proxyNftContract.getAmount(percentInBPS, inputAmount);
        expect(amount).to.eq(5);
      });
  
      it("get max bpst", async () => {
        const maxBPS = await proxyNftContract.getMaxBPS();
        expect(maxBPS).to.eq(10000);
      });
  
      it("get get next proxy tokenId to be assigned ", async () => {
        const nextId = await proxyNftContract.getNextTokenId();
        expect(nextId).to.eq(1);
      });
  
    });
  
    describe("royalty and mintfees functions", () => {
      it("get default royalty percentage", async () => {
        const royaltyPercent = await proxyNftContract.defaultRoyaltyPercentage();
        expect(defaultRoyaltyPercent).to.eq(royaltyPercent);
      });
  
      it("set new default royalty percentage", async () => {
        const newRoyaltyPercent = 200;
        const royaltyPercentBfr = await proxyNftContract.defaultRoyaltyPercentage();
        await proxyNftContract.setDefaultRoyaltyPercentage(newRoyaltyPercent);
        const royaltyPercentAft = await proxyNftContract.defaultRoyaltyPercentage();
        expect(royaltyPercentBfr).to.eq(defaultRoyaltyPercent);
        expect(royaltyPercentAft).to.eq(newRoyaltyPercent);
      });
      
      it("get default royalty address", async () => {
        const royaltyAddr = await proxyNftContract.defaultRoyaltyAddress();
        expect(royaltyAddr).to.eq(royaltyAddress);
      });
  
      it("set new default royalty address", async () => {
        const newRoyaltyAddress = accounts[5].address;
        const royaltyAddressBfr = await proxyNftContract.defaultRoyaltyAddress();
        await proxyNftContract.setDefaultRoyaltyAddress(newRoyaltyAddress);
        const royaltyAddressAft = await proxyNftContract.defaultRoyaltyAddress();
        expect(royaltyAddressBfr).to.eq(royaltyAddress);
        expect(royaltyAddressAft).to.eq(newRoyaltyAddress);
      });

      it("throws if Zero address is passed as new royalty address", async () => {
        await expect(
            proxyNftContract.setDefaultRoyaltyAddress(ZERO_ADDRESS)).to.be.revertedWith('invalid royalty address');
      });

      it("get default mint fees receiver address (contract owner)", async () => {
        const mintFeeRecAddress = await proxyNftContract.mintFeesReceiverAddress();
        expect(owner.address).to.eq(mintFeeRecAddress);
      });
  
      it("set new mint receiver address", async () => {
        const newMintFreeRecvAddress = accounts[5].address;
        const mintFeeRecvBfr = await proxyNftContract.mintFeesReceiverAddress();
        await proxyNftContract.setMintFeesReceiverAddress(newMintFreeRecvAddress);
        const mintFeeRecvAft = await proxyNftContract.mintFeesReceiverAddress();
        expect(mintFeeRecvBfr).to.eq(owner.address);
        expect(mintFeeRecvAft).to.eq(newMintFreeRecvAddress);
      });

      it("throws if Zero address is passed as new mint receiver address", async () => {
        await expect(
            proxyNftContract.setMintFeesReceiverAddress(ZERO_ADDRESS)).to.be.revertedWith('invalid address');
      });

     
      it("royalty and mint fee receiver setters can be called by owner only", async () => {
        await expect(
          proxyNftContract.connect(minter1).setDefaultRoyaltyPercentage(100)).to.be.revertedWith('Ownable: caller is not the owner');
      
        await expect(
          proxyNftContract.connect(minter1).setDefaultRoyaltyAddress(accounts[5].address)).to.be.revertedWith('Ownable: caller is not the owner');
  
  
        await expect(
          proxyNftContract.connect(minter1).setMintFeesReceiverAddress(accounts[5].address)).to.be.revertedWith('Ownable: caller is not the owner');
         
      });
    });
  
  
    describe("set wearables detais on chain", () => {
      
      it("set wearable data successfully", async () => {
        await proxyNftContract.setWearableNFTsData(
          ["ipfsHash1","ipfsHash2","ipfsHash3"],
          ["1000000000000000000",2000,3000],
          [
            [[sampleERC1155ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
            [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
            [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
          ],
          [5,5,5]
        );
        const _wearable1 = await proxyNftContract.wearablesData(1);
        const _wearable2 = await proxyNftContract.wearablesData(2);
        const _wearable3 = await proxyNftContract.wearablesData(3);
  
        expect(_wearable1.ipfsHash).to.eq("ipfsHash1");
        expect(_wearable2.ipfsHash).to.eq("ipfsHash2");
        expect(_wearable3.ipfsHash).to.eq("ipfsHash3");
        expect(_wearable1.price).to.eq("1000000000000000000");
        expect(_wearable2.price).to.eq(2000);
        expect(_wearable3.price).to.eq(3000);
        expect(_wearable1.copies).to.eq(5);
        expect(_wearable2.copies).to.eq(5);
        expect(_wearable3.copies).to.eq(5);
  
        const _wearable1Delegate1 = await proxyNftContract.wearableDelegates(1,0);
        const _wearable2Delegate2 = await proxyNftContract.wearableDelegates(2,1);
        const _wearable3Delegate1 = await proxyNftContract.wearableDelegates(3,0);
        
        expect(_wearable1Delegate1.contractAddress).to.eq(sampleERC1155ContractAddress);
        expect(_wearable2Delegate2.contractAddress).to.eq(sampleERC1155ContractAddress);
        expect(_wearable3Delegate1.contractAddress).to.eq(sampleERC1155ContractAddress);
        expect(_wearable1Delegate1.tokenId).to.eq(1);
        expect(_wearable2Delegate2.tokenId).to.eq(3);
        expect(_wearable3Delegate1.tokenId).to.eq(1);
        expect(_wearable1Delegate1.contractAddress).to.eq(sampleERC1155ContractAddress);
        expect(_wearable2Delegate2.contractAddress).to.eq(sampleERC1155ContractAddress);
        expect(_wearable3Delegate1.contractAddress).to.eq(sampleERC1155ContractAddress);
        expect(_wearable1Delegate1.quantity).to.eq(1);
        expect(_wearable2Delegate2.quantity).to.eq(1);
        expect(_wearable3Delegate1.quantity).to.eq(1);
  
        const wearableCount = await proxyNftContract.wearableNFTsCount();
        expect(wearableCount).to.eq(3);
  
      });
  
      it("throw if input arrays of set wearable data are not of same length", async () => {
        await expect(
          proxyNftContract.setWearableNFTsData(
            ["ipfsHash1","ipfsHash2"],
            ["1000000000000000000",2000,3000],
            [
              [[sampleERC1155ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
            ],
            [5,5,5]
          )).to.be.revertedWith('array lengths mismatch');
  
        await expect(
          proxyNftContract.setWearableNFTsData(
            ["ipfsHash1","ipfsHash2", "ipfsHash3"],
            ["1000000000000000000",2000],
            [
              [[sampleERC1155ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
            ],
            [5,5,5]
          )).to.be.revertedWith('array lengths mismatch');
        
  
        await expect(
          proxyNftContract.setWearableNFTsData(
            ["ipfsHash1","ipfsHash2", "ipfsHash3"],
            ["1000000000000000000",2000,1000],
            [
              [[sampleERC1155ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
            ],
            [5,5]
          )).to.be.revertedWith('array lengths mismatch');
      });
  
      it("throw if input array of delegate has invalid contract address", async () => {
  
        await expect(
          proxyNftContract.setWearableNFTsData(
            ["ipfsHash1","ipfsHash2","ipfsHash3"],
            ["1000000000000000000",2000,3000],
            [
              [[ZERO_ADDRESS,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
            ],
            [5,5,5]
        )).to.be.reverted;//With('invalid contractaddress');
      });
  
      it("throw if input array of delegate has ERC721 token with qty more than 1", async () => {
  
        await expect(
          proxyNftContract.setWearableNFTsData(
            ["ipfsHash1","ipfsHash2","ipfsHash3"],
            ["1000000000000000000",2000,3000],
            [
              [[sampleERC721ContractAddress,1, 2,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
            ],
            [5,5,5]
        )).to.be.revertedWith('ERC721 qty must be 1');
      });

        it("throw if delegate contract address standard cannot be identified", async () => {
          await expect( 
            proxyNftContract.setWearableNFTsData(
            ["ipfsHashx"],
            ["1000000000000000000"],
            [
              [[ZERO_ADDRESS,1, 1,'decentraland' ]],
            ],
            [5]
          )).to.be.reverted;
        });
  
    });
  
    describe("update wearable price", () => {
      it("updated wearable price successfully", async () => {
        await proxyNftContract.setWearableNFTsData(
            ["ipfsHash1","ipfsHash2","ipfsHash3"],
            ["1000000000000000000",2000,3000],
            [
              [[sampleERC1155ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
              [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
            ],
            [5,5,5]
          );  
        const _wearable1Bfr = await proxyNftContract.wearablesData(1);
        const _wearable2Bfr = await proxyNftContract.wearablesData(2);
        await proxyNftContract.updateWearablePricesByBatch([1,2],['150000000000000', '20000000000000000000000']);
        const _wearable1Aft = await proxyNftContract.wearablesData(1);
        const _wearable2Aft = await proxyNftContract.wearablesData(2);
  
        expect(_wearable1Bfr.price).to.eq("1000000000000000000");
        expect(_wearable2Bfr.price).to.eq("2000");
          
        expect(_wearable1Aft.price).to.eq("150000000000000");
        expect(_wearable2Aft.price).to.eq("20000000000000000000000");
        });
  
        it("only owner can update wearable price ", async () => {
          await proxyNftContract.setWearableNFTsData(
              ["ipfsHash1","ipfsHash2","ipfsHash3"],
              ["1000000000000000000",2000,3000],
              [
                [[sampleERC1155ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
                [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
                [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
              ],
              [5,5,5]
            );  
          
          await expect(
            proxyNftContract.connect(minter1).updateWearablePricesByBatch([1,2],['150000000000000', '20000000000000000000000']))
            .to.be.revertedWith('Ownable: caller is not the owner');
        
        });
    });
  
    describe("minting proxy token", () => {
      beforeEach(async () => {
        await proxyNftContract.setWearableNFTsData(
                ["ipfsHash1","ipfsHash2","ipfsHash3"],
                ["1000000000000000000",2000,3000],
                [
                  [[sampleERC721ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
                  [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
                  [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
                ],
                [5,5,5]
        );
      });

      it("mint proxy token successfully", async () => {
        const balanceMintFeeRecv = await provider.getBalance(owner.address);
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
        const proxyTokenDetails = await proxyNftContract.getProxyTokenData(1);
        const proxyWearableData = proxyTokenDetails[1];
        const proxyDelegates = proxyTokenDetails[0];
        const balanceMintFeeRecvAft = await provider.getBalance(owner.address);
       
        expect(proxyWearableData.price).to.eq('1000000000000000000');
        expect(proxyWearableData.ipfsHash).to.eq('ipfsHash1');
        expect(proxyWearableData.copies).to.eq(5);
        expect(proxyWearableData.mintCount).to.eq(1);
        expect(BigInt(balanceMintFeeRecvAft)).to.eq(BigInt(balanceMintFeeRecv) + BigInt('1000000000000000000'));

        expect(proxyDelegates[0].contractAddress).to.eq(sampleERC721ContractAddress);
        expect(proxyDelegates[0].tokenId).to.eq(1);
        expect(proxyDelegates[0].quantity).to.eq(1);
        expect(proxyDelegates[0].metaverseName).to.eq('decentraland');
        expect(proxyDelegates[1].contractAddress).to.eq(sampleERC1155ContractAddress);
        expect(proxyDelegates[1].tokenId).to.eq(2);
        expect(proxyDelegates[1].quantity).to.eq(1);
        expect(proxyDelegates[1].metaverseName).to.eq('decentraland');

        const nextProxyTokenId = await proxyNftContract.getNextTokenId();
        expect(nextProxyTokenId).to.eq(2);
        const tokenURI = await proxyNftContract.tokenURI(1);
        expect(tokenURI).to.eq(baseUri + 1);
        const proxyDelegate1ByFunc = await proxyNftContract.delegateTokens(1);
        expect(proxyDelegate1ByFunc[0].contractAddress).to.eq(sampleERC721ContractAddress);
        expect(proxyDelegate1ByFunc[0].tokenId).to.eq(1);
        expect(proxyDelegate1ByFunc[0].quantity).to.eq(1);
        expect(proxyDelegate1ByFunc[0].metaverseName).to.eq('decentraland');
        await proxyNftContract.connect(minter1).mint(2, {value: "2000"});
        await proxyNftContract.connect(minter1).mint(2, {value: "2000"});
        await proxyNftContract.connect(minter1).mint(1, {value: "1000000000000000000"});
        await proxyNftContract.connect(minter2).mint(2, {value: "2000"});


        const balance1 = await proxyNftContract.wearableBalances(minter1Address,1); 
        expect(balance1).to.eq(2);
        const balance2 = await proxyNftContract.wearableBalances(minter1Address,2); 
        expect(balance2).to.eq(2);
        const balance3= await proxyNftContract.wearableBalances(minter1Address,3); 
        expect(balance3).to.eq(0);
        await proxyNftContract.connect(minter1).transferFrom(minter1Address, minter2Address, 1);
        const balanceAft1 = await proxyNftContract.wearableBalances(minter1Address,1); 
        const balanceMinter2Aft1 = await proxyNftContract.wearableBalances(minter2Address,1); 
        expect(balanceAft1).to.eq(1);
        expect(balanceMinter2Aft1).to.eq(1);

        const tokens = [];
        const totalBalance = await proxyNftContract.balanceOf(minter2Address);
        for(let i = 0; i< parseInt(totalBalance); i++){
          const token = await proxyNftContract.tokenOfOwnerByIndex(minter2Address, i)
          tokens.push(token);
        } 
  
      });

      it("throw if wearableId passed in mint doesn't exist ", async () => {
        await expect(
          proxyNftContract.connect(minter1).mint(10, {from: minter1Address, value: "1000000000000000000"}))
          .to.be.revertedWith("wearableId doesn't exist");
      });

      it("throw if max tokens are minted for wearableId passed in mint ", async () => {
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});

        await expect(
          proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"}))
          .to.be.revertedWith("max tokens minted");
      });
      
      it("throw if incorrect ETH amount sent for minting", async () => {
        await expect(
          proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "100000000000000000"}))
          .to.be.revertedWith("invalid amount");
      });

      it("emit MINT event on successful minting", async () => {
        await expect(
          proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"}))
          .to.emit(proxyNftContract, "Mint")
          .withArgs(1,1,minter1Address);
      });
  
    });

    describe("bundle proxy token", () => {
      beforeEach(async () => {
        await proxyNftContract.setWearableNFTsData(
                ["ipfsHash1","ipfsHash2","ipfsHash3"],
                ["1000000000000000000",2000,3000],
                [
                  [[sampleERC721ContractAddress,5,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
                  [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 2,'decentraland' ]],
                  [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
                ],
                [5,5,5]
        );
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
        await proxyNftContract.connect(minter1).mint(2, {from: minter1Address, value: "2000"});
        await proxyNftContract.connect(minter2).mint(2, {from: minter2Address, value: "2000"});
        await proxyNftContract.connect(minter2).mint(3, {from: minter2Address, value: "3000"});
      });

      it("bundle delegate tokens successfully when delegate tokens owned by proxy owner", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
        await proxyNftContract.connect(minter1).bundle(
          DelegateData,
          2,
          {
            from: minter1Address
          }
        );
        const contractBalanceToken2  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 2);
        const contractBalanceToken3  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 3);
        const minterBalanceToken2  = await sampleERC1155Contract.balanceOf(minter1Address, 2);
        const minterBalanceToken3  = await sampleERC1155Contract.balanceOf(minter1Address, 3);
        expect(contractBalanceToken2).to.eq(1);
        expect(contractBalanceToken3).to.eq(2);
        expect(minterBalanceToken2).to.eq(4);
        expect(minterBalanceToken3).to.eq(3);

        const DelegateData2= [];
        DelegateData2.push(
          [sampleERC1155ContractAddress,1,1,'decentraland'],
          [sampleERC1155ContractAddress,3,1,'decentraland'],
        );

        await sampleERC1155Contract.connect(minter2).setApprovalForAll(proxyNftContract.address, true);
        await proxyNftContract.connect(minter2).bundle(
          DelegateData,
          3,
          {
            from: minter2Address
          }
        );
        await proxyNftContract.connect(minter2).bundle(
          DelegateData2,
          4,
          {
            from: minter2Address
          }
        );
        const contractBalance2Token2  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 2);
        const contractBalance2Token1  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 1);
        const contractBalance2Token3  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 3);
        const minterBalance2Token1  = await sampleERC1155Contract.balanceOf(minter2Address, 1);
        const minterBalance2Token2  = await sampleERC1155Contract.balanceOf(minter2Address, 2);
        const minterBalance2Token3  = await sampleERC1155Contract.balanceOf(minter2Address, 3);
        expect(contractBalance2Token1).to.eq(1);
        expect(contractBalance2Token2).to.eq(2);
        expect(contractBalance2Token3).to.eq(5);
        expect(minterBalance2Token1).to.eq(4);
        expect(minterBalance2Token2).to.eq(4);
        expect(minterBalance2Token3).to.eq(2);

        const tokenBalanceProxy1Token1 = await proxyNftContract.tokenBalances(1,sampleERC1155ContractAddress, 2);
        const tokenBalanceProxy1Token5 = await proxyNftContract.tokenBalances(1,sampleERC721ContractAddress, 5);
        const tokenBalanceProxy2Token2 = await proxyNftContract.tokenBalances(2,sampleERC1155ContractAddress, 2);
        const tokenBalanceProxy2Token3 = await proxyNftContract.tokenBalances(2,sampleERC1155ContractAddress, 3);
        const tokenBalanceProxy3Token2 = await proxyNftContract.tokenBalances(3,sampleERC1155ContractAddress, 2);
        const tokenBalanceProxy3Token3 = await proxyNftContract.tokenBalances(3,sampleERC1155ContractAddress, 3);
        const tokenBalanceProxy4Token1 = await proxyNftContract.tokenBalances(4,sampleERC1155ContractAddress, 1);
        const tokenBalanceProxy4Token3 = await proxyNftContract.tokenBalances(4,sampleERC1155ContractAddress, 3);
        expect(tokenBalanceProxy1Token1).to.eq(0);
        expect(tokenBalanceProxy1Token5).to.eq(0);
        expect(tokenBalanceProxy2Token2).to.eq(1);
        expect(tokenBalanceProxy2Token3).to.eq(2);
        expect(tokenBalanceProxy3Token2).to.eq(1);
        expect(tokenBalanceProxy3Token3).to.eq(2);
        expect(tokenBalanceProxy4Token1).to.eq(1);
        expect(tokenBalanceProxy4Token3).to.eq(1);

        const _wearable1 = await proxyNftContract.wearablesData(1);
        const _wearable2 = await proxyNftContract.wearablesData(2);
        const _wearable3 = await proxyNftContract.wearablesData(3);
        expect(_wearable1.mintCount).to.eq(1);
        expect(_wearable2.mintCount).to.eq(2);
        expect(_wearable3.mintCount).to.eq(1);
      });

      it("bundle delegate tokens successfully when delegate tokens owned by bundler", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(bundler).setApprovalForAll(proxyNftContract.address, true);
        await proxyNftContract.connect(bundler).bundle(
          DelegateData,
          2,
          {
            from: bundlerAddress
          }
        );
        const contractBalanceToken2  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 2);
        const contractBalanceToken3  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 3);
        const bundlerBalanceToken2  = await sampleERC1155Contract.balanceOf(bundlerAddress, 2);
        const bundlerBalanceToken3  = await sampleERC1155Contract.balanceOf(bundlerAddress, 3);
        expect(contractBalanceToken2).to.eq(1);
        expect(contractBalanceToken3).to.eq(2);
        expect(bundlerBalanceToken2).to.eq(4);
        expect(bundlerBalanceToken3).to.eq(3);
      });

      it("bundle delegates by bundler - unbundle delegates by proxy owner - again do bundle delegate tokens by proxy owner successfully  ", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(bundler).setApprovalForAll(proxyNftContract.address, true);
        await proxyNftContract.connect(bundler).bundle(
          DelegateData,
          2
        );
        await proxyNftContract.connect(minter1).unBundle(
          DelegateData,
          2
        );
        await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
        await proxyNftContract.connect(minter1).bundle(
          DelegateData,
          2
        );
        const contractBalanceToken2  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 2);
        const contractBalanceToken3  = await sampleERC1155Contract.balanceOf(proxyNftContract.address, 3);
        const bundlerBalanceToken2  = await sampleERC1155Contract.balanceOf(bundlerAddress, 2);
        const bundlerBalanceToken3  = await sampleERC1155Contract.balanceOf(bundlerAddress, 3);
        expect(contractBalanceToken2).to.eq(1);
        expect(contractBalanceToken3).to.eq(2);
        expect(bundlerBalanceToken2).to.eq(4);
        expect(bundlerBalanceToken3).to.eq(3);
      });


      it("throw if bundle delegate tokens is called by non - bundler or proxy owner", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(minter2).setApprovalForAll(proxyNftContract.address, true);
        await expect(
          proxyNftContract.connect(minter2).bundle(
            DelegateData,
            2,
            {
              from: minter2Address
            }
          )).to.be.revertedWith('caller neither bundler nor owner');
        
      });

      it("throw if tokens passed in delegate data is not assigned to proxy token", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,1,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
        await expect(
          proxyNftContract.connect(minter1).bundle(
            DelegateData,
            2,
            {
              from: minter1Address
            }
          )).to.be.revertedWith('delegate not assigned to proxy');
        
      });

      it("throw if tokens quantity passed in delegate data is more than quantity assigned to proxy token", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,2,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
        await expect(
          proxyNftContract.connect(minter1).bundle(
            DelegateData,
            2,
            {
              from: minter1Address
            }
          )).to.be.revertedWith('assigned delegate qty exceeds');
        
      });

      it("throw if all delegate tokens belonging to proxy token are already added to contract ", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
        await proxyNftContract.connect(minter1).bundle(
          DelegateData,
          2,
          {
            from: minter1Address
          }
        );
        await expect(
          proxyNftContract.connect(minter1).bundle(
            DelegateData,
            2,
            {
              from: minter1Address
            }
          )).to.be.revertedWith('assigned delegate qty exceeds');
        
      });

      it("emit Bundled event on successful bundling of tokens", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
        await expect(
          proxyNftContract.connect(minter1).bundle(
            DelegateData,
            2,
            {
              from: minter1Address
            }
          )).to.emit(proxyNftContract, 'Bundled');
      });

    });

    describe("unbundle proxy token", () => {
      beforeEach(async () => {
        await proxyNftContract.setWearableNFTsData(
                ["ipfsHash1","ipfsHash2","ipfsHash3"],
                ["1000000000000000000",2000,3000],
                [
                  [[sampleERC721ContractAddress,5,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
                  [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 2,'decentraland' ]],
                  [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
                ],
                [5,5,5]
        );
        await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
        await proxyNftContract.connect(minter1).mint(2, {from: minter1Address, value: "2000"});
        await proxyNftContract.connect(minter2).mint(2, {from: minter2Address, value: "2000"});
        await proxyNftContract.connect(minter2).mint(3, {from: minter2Address, value: "3000"});
        const DelegateData = [];
          DelegateData.push(
            [sampleERC1155ContractAddress,2,1,'decentraland'],
            [sampleERC1155ContractAddress,3,2,'decentraland'],
          );
          
          await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
          await proxyNftContract.connect(minter1).bundle(
            DelegateData,
            2,
            {
              from: minter1Address
            }
          );
          const DelegateData2= [];
          DelegateData2.push(
            [sampleERC1155ContractAddress,1,1,'decentraland'],
            [sampleERC1155ContractAddress,3,1,'decentraland'],
          );
          await sampleERC1155Contract.connect(minter2).setApprovalForAll(proxyNftContract.address, true);
          await proxyNftContract.connect(minter2).bundle(
            DelegateData2,
            4,
            {
              from: minter2Address
            }
          );
          const DelegateData3= [];
          DelegateData3.push(
            [sampleERC721ContractAddress,5,1,'decentraland']
          );
          await sampleERC721Contract.connect(bundler).approve(proxyNftContract.address, 5);
          await proxyNftContract.connect(bundler).bundle(
            DelegateData3,
            1
          );
      });

      it("unbundle delegate tokens successfully", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        const DelegateData2 = [];
        DelegateData2.push(
          [sampleERC721ContractAddress,5,1,'decentraland']
        );

        const erc721Token5OwnerBfr = await sampleERC721Contract.ownerOf(5);
        const token2BalanceBfr = await sampleERC1155Contract.balanceOf(minter1Address,2);
        const token3BalanceBfr = await sampleERC1155Contract.balanceOf(minter1Address,3);
        await proxyNftContract.connect(minter1).unBundle(
          DelegateData,
          2
        );
        await proxyNftContract.connect(minter1).unBundle(
          DelegateData2,
          1
        );
        const erc721Token5OwnerAft = await sampleERC721Contract.ownerOf(5);
        const token2BalanceAft = await sampleERC1155Contract.balanceOf(minter1Address,2);
        const token3BalanceAft = await sampleERC1155Contract.balanceOf(minter1Address,3);
        expect(token2BalanceAft).to.eq(parseInt(token2BalanceBfr) + 1);
        expect(token3BalanceAft).to.eq(parseInt(token3BalanceBfr) + 2);
        expect(erc721Token5OwnerBfr).to.eq(proxyNftContract.address);
        expect(erc721Token5OwnerAft).to.eq(minter1Address);


        const proxyToken2BalanceAft = await proxyNftContract.tokenBalances(1,sampleERC1155ContractAddress,2);
        const proxyToken3BalanceAft = await proxyNftContract.tokenBalances(1,sampleERC1155ContractAddress,3);
        expect(proxyToken2BalanceAft).to.eq(0);
        expect(proxyToken3BalanceAft).to.eq(0);

      });

      it("tranferred Proxy token unbundles delegate tokens successfully", async () => {
        const ownerBfr = await proxyNftContract.ownerOf(2);
        await proxyNftContract.connect(minter1).transferFrom(minter1Address, minter2Address, 2);
        const ownerAft = await proxyNftContract.ownerOf(2);
        expect(ownerBfr).to.eq(minter1Address);
        expect(ownerAft).to.eq(minter2Address);

        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        const token2BalanceBfr = await sampleERC1155Contract.balanceOf(minter2Address,2);
        const token3BalanceBfr = await sampleERC1155Contract.balanceOf(minter2Address,3);
        await proxyNftContract.connect(minter2).unBundle(
          DelegateData,
          2
        );
        const token2BalanceAft = await sampleERC1155Contract.balanceOf(minter2Address,2);
        const token3BalanceAft = await sampleERC1155Contract.balanceOf(minter2Address,3);
        expect(token2BalanceAft).to.eq(parseInt(token2BalanceBfr) + 1);
        expect(token3BalanceAft).to.eq(parseInt(token3BalanceBfr) + 2);

        const proxyToken2BalanceAft = await proxyNftContract.tokenBalances(1,sampleERC1155ContractAddress,2);
        const proxyToken3BalanceAft = await proxyNftContract.tokenBalances(1,sampleERC1155ContractAddress,3);
        expect(proxyToken2BalanceAft).to.eq(0);
        expect(proxyToken3BalanceAft).to.eq(0);

      });

      it("throw if unbundle delegate tokens is called by non proxy owner", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await expect(
          proxyNftContract.connect(minter2).unBundle(
            DelegateData,
            2
          )).to.be.revertedWith('caller not proxy owner');
      });

      it("throw if delegate tokens mentioned in args are not assigned to proxy token", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,1,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await expect(
          proxyNftContract.connect(minter1).unBundle(
            DelegateData,
            2
          )).to.be.revertedWith('delegate not assigned to proxy');
      });

      it("throw if quantity of delegate tokens mentioned in args is more than quantity assigned to proxy token", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,2,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await expect(
          proxyNftContract.connect(minter1).unBundle(
            DelegateData,
            2
          )).to.be.revertedWith('qty exceeds balance');
      });

      it("throw if unBundle is called on proxy token with 0 token balance", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        await proxyNftContract.connect(minter1).unBundle(
          DelegateData,
          2
        );
        await expect(
          proxyNftContract.connect(minter1).unBundle(
            DelegateData,
            2
          )).to.be.revertedWith('qty exceeds balance');
      });
    
      it("emit UnBundled event on successful unbundling of tokens", async () => {
        const DelegateData = [];
        DelegateData.push(
          [sampleERC1155ContractAddress,2,1,'decentraland'],
          [sampleERC1155ContractAddress,3,2,'decentraland'],
        );
        
        await sampleERC1155Contract.connect(minter1).setApprovalForAll(proxyNftContract.address, true);
        await expect(
          proxyNftContract.connect(minter1).unBundle(
            DelegateData,
            2,
            {
              from: minter1Address
            }
          )).to.emit(proxyNftContract, 'Unbundled');
      });

    });

});



  // describe("setwearable data", () => {
  //   it("set wearable data successfully", async () => {

  //     await proxyNftContract.setWearableNFTsData(
  //       ["a","a","a"],
  //       ["1000000000000000000",2000,3000],
  //       [
  //         [[sampleERC1155ContractAddress,1,  1,'decentraland' ],[sampleERC1155ContractAddress,2, 1,'decentraland' ]],
  //         [[sampleERC1155ContractAddress,2, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]],
  //         [[sampleERC1155ContractAddress,1, 1,'decentraland' ],[sampleERC1155ContractAddress,3, 1,'decentraland' ]]
  //       ],
  //       [5,5,5]
  //     );
  //     const _wearable1 = await proxyNftContract.wearablesData(1);

  //     const balanceMinterBfr = await provider.getBalance(minter1Address);
  //     console.log("balanceMinterBfr",balanceMinterBfr);
  //     await proxyNftContract.connect(minter1).mint(1, {from: minter1Address, value: "1000000000000000000"});
  //     const balanceMinteraft = await provider.getBalance(minter1Address);
  //     console.log("balanceMinteraft",balanceMinteraft);
  //     // const totalSupply = await proxyNftContract.totalSupply();
  //     // const proxyDelegate1 = await proxyNftContract.proxyDelegates(1);
  //     // console.log("proxyDelegate1",proxyDelegate1);
  //     const proxyNFT = await proxyNftContract.getProxyTokenData(1);
  //     console.log("proxyNFT",proxyNFT);
  //     const proxyNFTUri = await proxyNftContract.tokenURI(1);
  //     console.log("proxyNFT", proxyNFT,proxyNFTUri); 

  //     const DelegateData = [];
  //     DelegateData.push(
  //       [sampleERC1155ContractAddress,1,1,'decentraland'],
  //       [sampleERC1155ContractAddress,2,1,'decentraland'],
  //     );
      
  //     await sampleERC1155Contract.connect(bundler).setApprovalForAll(proxyNftContract.address, true);
  //     await proxyNftContract.connect(bundler).bundle(
  //       DelegateData,
  //       1,
  //       {
  //         from: bundlerAddress
  //       }
  //     )

  //     const ownerAft = await sampleERC1155Contract.balanceOfBatch([proxyNftContract.address,proxyNftContract.address,proxyNftContract.address],[1,2,3])
  //     console.log('bal',ownerAft);
  //     const proxyDelegate2 = await proxyNftContract.proxyDelegates(1,1);
  //     console.log('proxyDelegate2',proxyDelegate2);

  //     await proxyNftContract.connect(minter1).unBundle(
  //       DelegateData,
  //       1,
  //       {
  //         from: minter1Address
  //       }
  //     );

  //     const ownerAft2 = await sampleERC1155Contract.balanceOfBatch([proxyNftContract.address,proxyNftContract.address,proxyNftContract.address],[1,2,3])
  //     console.log('bal',ownerAft2);
  //     const ownerAft3 = await sampleERC1155Contract.balanceOfBatch([minter1Address,minter1Address,minter1Address],[1,2,3])
  //     console.log('bal',ownerAft3);
  //     const proxyDelegate3 = await proxyNftContract.proxyDelegates(1,1);
  //     const wearableData = await proxyNftContract.wearablesData(1);

  //     console.log('proxyDelegate3',proxyDelegate3,wearableData);

  //   });

  // });

